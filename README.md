# Wozmon for Planck 6502

This is an adaptation of [Ben Eater's version of the Wozmon
monitor](https://youtu.be/7M8LvMtdcgY?si=bZvkbyaQ7Z54Lh2u). I've
edited it to run on [Jonathan Focher's Planck
6502](https://planck6502.com/) computer. This mostly means that
I added in Jonathan's ACIA serial IO routines where appropriate.

Unlike Ben Eater's version, I've left the monitor at $8000, as I
couldn't shrink it down enough to fit in the same 255 bytes that
he managed. 

This is a _bare_ ROM. There is very little here; just the
monitor. There is no way to jump to Forth, for example. 

I did not create a Makefile for this. Instead, there are two
very basic scripts: `buildwozmon.sh` and `burnwozmon.sh` which
should help you build a new ROM from the `wozmon_eater.s` file.

After starting up with the new ROM, you should see a `\` prompt.
To display a byte from RAM or ROM, enter its address. To display
a range, use a `.`, for example, `C000.C0FF`. To modify a byte
or series of bytes, give the start address followed by a colon,
like so: `AA00: `, followed by the bytes to be replaced. You can
also run code by giving an address range, followed by `R`, as in
`C000 R`. There are more details in the excellent video from Ben
referenced in the beginning of this article. There is also an
excellent introduction to Wozmon at [SB-Projects - Projects -
Apple 1 Woz
Monitor](https://www.sbprojects.net/projects/apple1/wozmon.php) 

This is mostly for fun, as everything that can be done from
Wozmon could also be done from Tali Forth itself.

To exit, power off or reset.



